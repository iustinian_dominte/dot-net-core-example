(function() {
    'use strict';

    angular
        .module('app')
        .controller('RunnerController', RunnerController);

    RunnerController.$inject = ['$http'];
    function RunnerController($http) {
        var vm = this;
        vm.runners = [];

        activate();
        ////////////////

        function activate() {
            $http.get('/api/runner')
                .then(function(response) {
                    vm.runners = response.data;
                })
                .catch(function(err) {
                    console.log(err);
                })
        }
    }
})();